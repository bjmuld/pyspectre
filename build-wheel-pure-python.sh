#!/bin/bash
set -e -x

#############################################################################
#############################################################################
###
###
###         REQUIRES:  $PKG_NAME
###
###
###         THIS WILL BUILD A WHEEL FOR PYTHON
###
###
#############################################################################
#############################################################################

# MOVE INTO MOUNTED DIRECTORY:
cd "${CI_PROJECT_DIR}"   ### remainder of this script uses abs paths, just in case


die()
{
	local _ret=$2
	test -n "$_ret" || _ret=1
	test "$_PRINT_HELP" = yes && print_help >&2
	echo "$1" >&2
	if [[ -v ocwd ]]; then
        cd $ocwd
    fi
	exit ${_ret}
}

[ -z ${PKG_NAME+x} ] && die 'you have not supplied $PKG_NAME (package name)'


# echo "***************** upgrading pip **********************"
pip install --upgrade pip

if [[ -r dev-requirements.txt ]]; then
    # echo "***************** installing dev-requirements.txt **********************"
    pip install -r dev-requirements.txt
fi

# echo "***************** building extensions **********************"
python setup.py build_ext

# echo "***************** building our wheel **********************"
pip wheel --no-deps -w wheelhouse/ ./

# echo "****************** list contents of wheelhouse ****************
ls -alh wheelhouse

## echo "***************** installing auditwheel **********************"
#pip install --upgrade auditwheel

## echo "***************** auditing wheels **********************"
#auditwheel repair wheelhouse/*$PKG_NAME*.whl -w ./wheelhouse/

# echo "***************** removing other wheels **********************"
find ./wheelhouse/* ! -name "*${PKG_NAME}*" -delete

ls -alh ${CI_PROJECT_DIR}/wheelhouse/

## echo "***************** test installing our wheel **********************"
#pip install -f wheelhouse --no-cache $PKG_NAME

## if there's a test script, run it....
#
#if [[ -r test.sh ]]; then
#    echo "***************** running test script **********************"
#    test_result=$(bash test.sh && echo "passed" || echo "failed")
#    if [[ "$test_result" == 'failed' ]]; then
#        exit 1
#    elif [[ "$test_result" == 'passed' ]]; then
#        exit 0
#    else
#        exit 1
#    fi
#fi
