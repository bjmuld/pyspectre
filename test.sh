#!/usr/bin/env bash

set -e -x

# do tests
######################################################################
pip install -U nose pytest tox

#"${PYBIN}/nosetests" $PKG_NAME
#teststr="$( ${PYBIN}/python -c \'import $PKG_NAME; result=True if "PSFDataSet" in dir(libpsf) else False; print(result)\' )"

ls -alh "${CI_PROJECT_DIR}/wheelhouse/"

# tox pytest

exit 0
