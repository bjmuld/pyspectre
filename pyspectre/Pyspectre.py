import signal
import time
import re
import random
import os
import shutil
import numpy as np
import libpsf
import tarfile
import threading
import string
import concurrent.futures
import subprocess
from copy import deepcopy

from io import BytesIO
from hashlib import sha1 as filehashfn

import waverunner

from scipy.interpolate import interp1d

from multiprocessing.managers import BaseManager
from multiprocessing import Lock

from .spectre.skill_interface import Accelerated_scl_interface

from . import default_spectre_binary, default_rcfile, default_spectre_env
from . import pyspectre_path, pyspectre_ckt_dir, pyspectre_sim_dir
from .spectre.replwrap_server import get_wrapped_spectre
from .spectre import format_vect, extend_pwl_defs_in_netlist, insert_analysis_block, gen_transient_analysis_block

_spectre_managed_models = False
_spectre_env_flag = False
_rcfile = None
_spectre_env = None
_spectre_binary = default_spectre_binary

local_sim_db_relpath = 'PYSPECTRE_AHDL_SIM'
local_ship_db_relpath = 'PYSPECTRE_AHDL_SHIP'

modfile_suffix = '_PYSPECTRE_ALTERED'
modfile_prefix = ''

transient_analysis_name = 'spectre_tran'

# # handle SIGINT from SyncManager object
# def mgr_sig_handler(signal, frame):
#     print('not closing the mgr')
#
# # initilizer for SyncManager
# def mgr_init():
#     signal.signal(signal.SIGINT, mgr_sig_handler)
#     #signal.signal(signal.SIGINT, signal.SIG_IGN) # <- OR do this to just ignore the signal
#     print('initialized mananger')


class LockManager(BaseManager):
    pass

try:
    LockManager.register('global_lock')
    global_manager = LockManager(address=('', 11223), authkey=b'iamjustalittlepyspectre')
    global_manager.connect()

except BaseException as e:
    # there is no lock server... start one:
    global_lock = Lock()
    LockManager.register('global_lock', callable=lambda: global_lock)
    global_manager = LockManager(address=('', 11223), authkey=b'iamjustalittlepyspectre')
    global_manager.start()

# def get_rcfile():
#     return _rcfile
#
#
# def get_spectre_env():
#     return _spectre_env
#
#
# def get_spectre_binary():
#     return _spectre_binary


def list_all_workdirs():
    try:
        return os.listdir(pyspectre_sim_dir)
    except:
        return []


def list_active_workdirs():
    active = [line.split() for line in
              subprocess.check_output(['bash', '-c', 'ps -ef | grep pyspectre']).decode().split('\n') if
              len(line.split()) == 23]
    return [(os.path.split(item[9])[-1], item[1]) for item in active]


def list_inactive_workdirs():
    all = list_all_workdirs()
    active = list_active_workdirs()
    return [item for item in all if item not in active]


def set_rcfile(filepath=None):
    global _rcfile

    if filepath is None:
        filepath = default_rcfile

    filespec = os.path.expandvars(os.path.expanduser(filepath))

    if os.path.isfile(filespec):
        _rcfile = filespec


def set_spectre_env(env=None):
    global _spectre_env

    if env is None:
        _spectre_env = default_spectre_env
    else:
        assert isinstance(env, (tuple, list))
        assert all([isinstance(item, tuple) for item in env])
        assert all([isinstance(item, str) for pairs in env for item in pairs])
        _spectre_env = env


def set_spectre_binary(exepath=None):
    global _spectre_binary

    if exepath is None:
        _spectre_binary = default_spectre_binary
    else:
        specpath = os.path.expandvars(os.path.expanduser(exepath))
        assert os.path.isfile(specpath)
        _spectre_binary = specpath


def _digest_file(filepath):
    return filehashfn(open(filepath, mode='rb').read().replace(' '.encode(), ''.encode())).hexdigest()


def _digest_string(string):
    return filehashfn(string.encode()).hexdigest()


def _get_patterns_for_internal_replacement(path_representation):
    return [r'^.\d*({})_.*[.]dep$'.format(path_representation)]


def hash_test(stimuli, time_vec):
    return str(filehashfn(
        str(tuple(np.concatenate([time_vec.ravel(), stimuli.ravel()]))).encode('utf-8')
    ).hexdigest()[:16])


def hash_tests(stimsets, time_vect):
    ids = []
    for tt in stimsets:
        ids.append(hash_test(tt, time_vect))
    return ids


class PyspectreUnresolvableError(ValueError):
    pass


class PwlInput(object):
    def __init__(self, name, max_n_points, parent=None):
        self.max_n_points = max_n_points
        self.name = name
        self.cname = '.'.join(name, parent) if parent else name
        self.default_value = None
        self.recent_value = None


class Parameter(object):
    def __init__(self, name, parent=None, ):
        if parent is not None:
            name = '.'.join(name, parent)
        self.name = name
        self.default_value = None
        self.recent_value = None


class Circuit(object):
    def __init__(self,
                 netlist=None,
                 simdirectory=None,
                 analysis_name=None,
                 spectre_args=None,
                 pwl_inputs=None,
                 parameters=None,
                 ):

        # settle netlist file
        if not os.path.isfile(netlist):
            # print('netlist \"{}\" is not file'.format(netlist))
            # print('cwd: \"{}\"'.format(os.getcwd()))
            if not os.path.isdir(simdirectory):
                # print('simdirectory \"{}\" is not directory'.format(simdirectory))
                # print('cwd: \"{}\"'.format(os.getcwd()))
                raise IOError(
                    'unable to find spice netlist: \"{}\" simdirectory: \"{}\"'.format(netlist, simdirectory))
            netlist = os.path.join(simdirectory, netlist)
            if not os.path.isfile(netlist):
                # print('netlist appended.  still not file: \"{}\"'.format(netlist))
                # print('cwd: \"{}\"'.format(os.getcwd()))
                raise IOError(
                    'unable to find spice netlist: netlist \"{}\" simdirectory: \"{}\"'.format(netlist, simdirectory))

        # settle simdir
        if simdirectory is not None and not os.path.isdir(simdirectory):
            simdirectory = os.path.dirname(netlist)

        if spectre_args is not None:
            if isinstance(spectre_args, list):
                spectre_args = ' '.join(spectre_args)
            elif isinstance(spectre_args, str):
                spectre_args = spectre_args

        if pwl_inputs is not None:
            if isinstance(pwl_inputs, PwlInput):
                pwl_inputs = tuple(pwl_inputs.cname, pwl_inputs)
            else:
                assert isinstance(pwl_inputs, (list, tuple))
                assert all([isinstance(item, PwlInput) for item in pwl_inputs])
                pwl_inputs = tuple(pwl_inputs)

        if parameters is not None:
            if isinstance(parameters, Parameter):
                parameters = tuple(parameters)
            else:
                assert isinstance(parameters, (list, tuple))
                assert all([isinstance(item, Parameter) for item in parameters])
                parameters = tuple(parameters)

        # build command
        # self.resolve_spectre_modes(spectre_output_mode, spectre_reader)

        # self.spicefilepath = os.path.abspath(netlist)
        with open(os.path.abspath(netlist), 'r') as f:
            self.netlist = f.read()
        self.simdir = simdirectory
        self.analysis_name = analysis_name
        self.spectre_args = spectre_args
        self.pwl_inputs = pwl_inputs
        self.parameters = parameters
        self.tarball = None
        self.id = str(hash(self))

    # def resolve_spectre_modes(self, spectre_output_mode, spectre_reader):
    #     if spectre_output_mode is None and spectre_reader is None:
    #         self.spectre_output_mode = 'psfbin'
    #         self.spectre_reader = 'libpsf'
    #
    #     elif spectre_reader is not None and spectre_output_mode is None:
    #         assert spectre_reader in avail_readers
    #         self.spectre_reader = spectre_reader
    #         if spectre_reader == 'decida':
    #             self.spectre_output_mode = 'nutascii'
    #         elif spectre_reader == 'libpsf':
    #             self.spectre_output_mode = 'psfbin'
    #         else:
    #             raise NotImplementedError
    #
    #     elif spectre_output_mode is not None and spectre_reader is None:
    #         assert spectre_output_mode in avail_output_modes
    #         self.spectre_output_mode = spectre_output_mode
    #         if spectre_output_mode == 'ascii':
    #             self.spectre_reader = 'nutascii'
    #         elif spectre_output_mode == 'psfbin':
    #             self.spectre_reader = 'libpsf'
    #         else:
    #             raise NotImplementedError
    #
    #     else:
    #         assert spectre_reader in avail_readers
    #         self.spectre_reader = spectre_reader
    #         assert spectre_output_mode in avail_output_modes
    #         self.spectre_output_mode = spectre_output_mode

    def pack_up(self):
        self.tarball = self._make_tarball()

    def unpack(self, path):
        if not path:
            path = os.getcwd()
        tbs = BytesIO(self.tarball)
        tbs.seek(0)
        tb = tarfile.open(fileobj=tbs, mode='r:bz2')
        tb.extractall(path=path)
        tb.close()

    def _make_tarball(self):
        tarball = BytesIO()
        tb = tarfile.open(fileobj=tarball, mode='w:bz2')
        for item in os.listdir(self.simdir):
            tb.add(os.path.join(self.simdir, item), arcname=item)
        tb.close()
        tarball.seek(0)
        return bytes(tarball.read())

    def __hash__(self):
        return int(_digest_string(''.join(self._hash_files())), 16)

    def _hash_files(self):
        file_hashes = {}
        file_hashes.update({'netlist': _digest_string(self.netlist)})

        if self.simdir is not None:
            for bdir, sdirs, files in os.walk(self.simdir):
                for ff in files:
                    filepath = os.path.join(bdir, ff)
                    file_hashes.update({filepath: _digest_file(filepath)})

        hashes = list(file_hashes.values())
        hashes.sort()
        return hashes

    def run_batch(self, *args, **kwargs):
        return run_batch(self, *args, **kwargs)

    def start_pyspectre(self):
        return Pyspectre(circuit=self, asynch_repl=False, logging=False, verbosity=0)


def do_batch_on_runner(runner, list_of_stimuli, list_of_times, signal_names, resample, verbosity, dtype, strobe=True):
    return runner.run_batch(
        list_of_stimuli=list_of_stimuli,
        list_of_times=list_of_times,
        signal_names=signal_names,
        resample=resample,
        verbosity=verbosity,
        dtype=dtype,
        strobe=strobe
    )


def do_batch_here(circuit, list_of_stimuli, list_of_times, signal_names, resample, verbosity, logging, asynch_repl, dtype,
                  shutdown_event=None, strobe=False):

    print('starting pyspectre...') if verbosity >= 4 else None
    if strobe and not all([np.all(item == list_of_times[0]) for item in list_of_times]):
        print('trying to strobe a batch with different time vectors... will split '
              'batch according to times')
        timeblocks = {}
        for i, time_vec in enumerate(list_of_times):
            timehash = hash(str(time_vec))
            if timehash not in timeblocks:
                timeblocks[timehash] = [i]
            else:
                timeblocks[timehash] += [i]
    else:
        timeblocks = {'onlyone': list(range(len(list_of_times)))}

    n_blocks = len(timeblocks.keys())
    all_times = [None]*len(list_of_stimuli)
    all_signals = [None]*len(list_of_stimuli)

    for block_ind, block in enumerate(timeblocks.values()):
        if n_blocks > 1:
            print('starting timeblock {}/{}'.format(block_ind, len(timeblocks))) if verbosity >= 1 else None

        strobe = np.min(np.diff(list_of_times[block[0]])) if strobe else None

        with Pyspectre(circuit=circuit, logging=logging, verbosity=verbosity, asynch_repl=asynch_repl,
                       shutdown_event=shutdown_event, strobe=strobe) as pyspectre:

            block_results = do_batch_on_runner(pyspectre, list_of_stimuli[block],
                                                      list_of_times[block], signal_names, resample,
                                                      verbosity, dtype)

            for i, result in enumerate(zip(*block_results)):
                all_times[block[i]] = result[0]
                all_signals[block[i]] = result[1]

    return all_times, all_signals


def run_batch(circuit, list_of_stimuli, list_of_times, signal_names='all', resample=False, strobe=True,
              parallel=False, n_workers=0, max_jobsize=10, verbosity=1, local=True, timeout=600,
              logging=False, asynch_repl='thread', dtype='float64'):

    if not signal_names or signal_names is None:
        signal_names = 'all'

    if strobe and not all([np.all(item == list_of_times[0]) for item in list_of_times]):
        print('trying to strobe a batch with different time vectors... will split '
              'batch according to times')

    timeblocks = {}
    for job_no, time_vec in enumerate(list_of_times):
        timekey = str(np.min(np.diff(time_vec)))
        if timekey not in timeblocks:
            timeblocks[timekey] = [job_no]
        else:
            timeblocks[timekey] += [job_no]

    all_times = [None] * len(list_of_stimuli)
    all_sigs = [None] * len(list_of_stimuli)

    all_ts = tuple(timeblocks.keys())
    ts_ordering = np.argsort(np.array([float(item) for item in all_ts]))[::-1]
    decreasing_ts = [all_ts[i] for i in ts_ordering]
    sims_by_block = [timeblocks[key] for key in decreasing_ts]

    if not parallel:

        for block in sims_by_block:

            block_results = do_batch_here(
                circuit=circuit,
                list_of_times=list_of_times[block],
                list_of_stimuli=list_of_stimuli[block],
                signal_names=signal_names,
                resample=resample,
                verbosity=verbosity,
                logging=logging,
                asynch_repl=asynch_repl,
                dtype=dtype,
                strobe=strobe,
            )

            for i, (block_times, sigs) in enumerate(zip(*block_results)):
                all_times[block[i]] = block_times
                all_sigs[block[i]] = sigs

    elif parallel:

        job_sets = []

        for block in sims_by_block:
            nsims = len(block)
            np.random.shuffle(block)
            # resort by expected duration::::
            by_duration = np.array(block)[np.argsort(list_of_times[block][:, -1])].reshape(2, -1).T
            by_duration[:, 1] = by_duration[:, 1][::-1]
            block = by_duration.flatten()

            expected_sims_per_worker = int(np.ceil(nsims / n_workers))

            if expected_sims_per_worker > max_jobsize:
                n_full_batches = int(np.ceil(nsims / max_jobsize))
                job_inds = np.array_split(block, n_full_batches, axis=0)
            else:
                njobs = min([n_workers, nsims])
                job_inds = np.array_split(block, njobs, axis=0)

            for inds in job_inds:
                job_sets.append(inds)

        if local:

            print('submitting {} batches'.format(len(job_inds))) if verbosity >= 1 else None
            otime = time.time()

            with concurrent.futures.ThreadPoolExecutor(max_workers=n_workers) as executor:
                shutdown = threading.Event()

                jobs = {}
                try:
                    for job_inds in job_sets:

                        jobs[executor.submit(
                            do_batch_here,
                            circuit=circuit,
                            list_of_times=list_of_times[job_inds],
                            list_of_stimuli=list_of_stimuli[job_inds],
                            signal_names=signal_names,
                            resample=resample,
                            verbosity=verbosity,
                            logging=logging,
                            asynch_repl=asynch_repl,
                            dtype=dtype,
                            strobe=strobe,
                        )] = job_inds

                    for future in concurrent.futures.as_completed(jobs):

                        # print('[pyspectre] completed batch {} of {}'.format(
                        # JOB_IDS.index(jid)+1, len(JOB_IDS))) if verbosity >= 1 else None

                        print('[pyspectre] {} batches remaining'.format(
                            sum([1 for j in jobs if not j.done() and not j.cancelled()]))) if verbosity >= 1 else None
                        try:
                            job_inds = jobs[future]
                            jtimes, jresponses = future.result()
                        except Exception as exc:
                            print('%r generated an exception: %s' % (job_inds, exc))
                        else:
                            for rsimind, rtime, rresponse in zip(job_inds, jtimes, jresponses):
                                all_times[rsimind] = rtime
                                all_sigs[rsimind] = rresponse

                except KeyboardInterrupt as e:
                    for future in jobs:
                        future.cancel()
                    shutdown.set()
                    raise e

            elapsed = time.time() - otime
            print('all batches took {:5.1f} s to complete'.format(elapsed))

        elif not local:
            raise  NotImplementedError
            # queuer = waverunner.Waverunner(notify_interval=0, polling_interval=0, verbosity=0)
            # queuer.start_service()
            #
            # jobids = []
            # circuit.solver = None
            # for inds in job_inds:
            #     jobids.append(queuer.queue_job(
            #         'run_batch',
            #         kwargs=do_batch_here_kwargs,
            #         timeout=timeout,
            #         # completion_hook=completion_hook,
            #     ))
            #
            # print('waiting for up to {} seconds for all jobs to complete'.format(timeout)) if verbosity >= 1 else None
            # queuer.block_for_queue(timeout=timeout)
            #
            # completed = queuer.get_completed_jobs(flush=False)
            # batch_results = []
            # for id in jobids:
            #     if id in [j.id for j in completed]:
            #
            #         job = completed[[j.id for j in completed].index(id)]
            #
            #         if batch_results is None:
            #             batch_results = job.result
            #         else:
            #             batch_results = np.concatenate([batch_results, job.result], axis=0)
            #
            # queuer.get_completed_jobs(flush=True)
            # queuer.stop_service()

    failed = [i for i in range(len(list_of_stimuli)) if all_sigs[i] is None]
    print('[pyspectre] {} simulations failed: {}'.format(len(failed), str(failed))) if verbosity >= 1 else None
    return all_times, all_sigs


class Pyspectre(object):

    def __init__(self,
                 circuit=None,
                 netlist=None,
                 simdirectory=None,
                 analysis_name=None,
                 spectreargs=None,
                 logging=False,
                 pwl_inputs=None,
                 asynch_repl='thread',
                 verbosity=1,
                 shutdown_event=None,
                 strobe=None,
                 ):
        self.verbosity = verbosity

        # def _signal_handler(self, signal, frame):
        #     shutdown_event.set()
        #     self.specter.join()
        #     self._cleanup()
        #     raise KeyboardInterrupt

        if strobe is not None:

            strobe == np.array(strobe)

            if strobe.size == 1:
                self.strobe_interval = strobe
                self.strobetimes = None

            elif strobe.size > 1:
                self.strobetimes = strobe
                self.strobe_interval = None

        else:
            self.strobetimes = None
            self.strobe_interval = None

        # if isinstance(threading.current_thread(), threading._MainThread):
        #     signal.signal(signal.SIGINT, _signal_handler)
        #     signal.signal(signal.SIGABRT, _signal_handler)
        #     signal.signal(signal.SIGTERM, _signal_handler)

        if circuit is None:
            circuit = Circuit(
                netlist=netlist,
                simdirectory=simdirectory,
                analysis_name=analysis_name if analysis_name else transient_analysis_name,
                spectre_args=spectreargs,
                pwl_inputs=pwl_inputs
            )
        self.circuit = deepcopy(circuit)
        self.pwl_inputs = circuit.pwl_inputs

        print('customizing netlist...') if self.verbosity >= 4 else None
        self.circuit.netlist = self._process_netlist_text(self.circuit.netlist)
        self.circuit_id = str(hash(self.circuit))

        if not analysis_name:
            analysis_name = self.circuit.analysis_name if self.circuit.analysis_name else transient_analysis_name
        self.analysis_name = analysis_name
        self.simdir = self.circuit.simdir
        self.spectre_args = self.circuit.spectre_args
        self.pwl_inputs = self.circuit.pwl_inputs
        self.logging = logging
        self.asynch_repl = asynch_repl
        self.shutdown_event = shutdown_event

        self.original_simdir = None
        self.analysis_type = None
        self.dcsolution = None
        self.working_path = None
        self.results_dir = None
        self.quit = False
        self.spectre = None
        self.interface = None
        self.fresh = True
        self.pid = None
        self.commandct = None
        self.spectre = None
        self.cmd = None
        self.reader = 'libpsf'
        self.strobe = True if strobe is None else strobe

        self._start_binary()

    def __del__(self):
        self._cleanup()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._cleanup()
        del self
        return

    def _cleanup(self):
        self.quit = True
        try:
            self.interface.quit()
        except:
            try:
                self.spectre.child.terminate()
            except:
                try:
                    self.spectre.child.kill(15)
                except:
                    try:
                        self.spectre.child.kill(9)
                    except:
                        try:
                            os.kill(self.spectre_pid, 9)
                            os.kill(self.bash_pid, 9)
                        except:
                            pass

        try:
            self.spectre.child.join()
        except:
            pass

        try:
            self.spectre.child.wait()
        except:
            pass

        try:
            shutil.rmtree(self.working_path)
        except:
            pass

        # try to purge sim path
        current_candidates = list_inactive_workdirs()
        from . import _candidates_for_purging as original_candidates

        for stale_dir in [item for item in original_candidates[0] if item in current_candidates and time.time()-original_candidates[1] > 60]:
            try:
                shutil.rmtree(os.path.join(pyspectre_sim_dir, stale_dir))
                print('[pyspectre] removed stale stimdir: {}'.format(stale_dir))
            except:
                pass

    def _start_binary(self):
        global _spectre_env_flag

        if self.spectre is not None and self.spectre.child.isalive:
            return None
        if self.quit:
            raise RuntimeError('tried starting a binary inside a dead Pyspectre instance')

        print('creating basic pyspectre dirs') if self.verbosity >= 4 else None
        # create/set shared working paths:
        os.makedirs(pyspectre_path, exist_ok=True)
        # os.makedirs(pyspectre_ahdl_lib, exist_ok=True)  # moving libraries to inside circuit #
        os.makedirs(pyspectre_sim_dir, exist_ok=True)
        os.makedirs(pyspectre_ckt_dir, exist_ok=True)

        # place circuit dir in cache
        cache_target = os.path.join(pyspectre_ckt_dir, str(self.circuit_id))
        if str(self.circuit_id) not in os.listdir(pyspectre_ckt_dir):
            print('trying to get lock to cache circuit') if self.verbosity >= 4 else None

            # cache miss... get a lock:
            global_manager.global_lock().acquire(timeout=120)
            if self.circuit_id in os.listdir(pyspectre_ckt_dir):
                # somebody else put it there...
                global_manager.global_lock().release()
            else:
                # it's still not there... cache it
                print('caching circuit') if self.verbosity >= 4 else None

                if self.circuit.tarball is not None:
                    # unpack circuit in correct cache directory
                    self.original_simdir = self.simdir
                    self.circuit.unpack(cache_target)

                else:
                    if os.path.isdir(self.simdir):
                        # copy path to correct cache directory
                        shutil.copy(self.simdir, pyspectre_ckt_dir)
                        shutil.move(os.path.join(pyspectre_ckt_dir, os.path.split(self.simdir)[-1]),
                                    cache_target)

                    # copy spicefile
                    os.makedirs(cache_target, exist_ok=True)
                    shutil.copy(self.spicefilepath, cache_target)

                with open(os.path.join(cache_target, str(self.circuit_id)+'.scs'), 'w') as f:
                    f.write(self.circuit.netlist)

                global_manager.global_lock().release()

        # setup working_path
        print('creating simulation dir') if self.verbosity >= 4 else None
        self.simid = ''.join(
            random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
            for _ in range(10))

        self.working_path = os.path.join(pyspectre_sim_dir, self.simid)
        if os.path.isdir(self.working_path):
            os.removedirs(self.working_path)
        os.makedirs(self.working_path)
        self.results_dir = os.path.join(self.working_path, 'psf')
        os.makedirs(self.results_dir, exist_ok=False)

        # hardlink everything into sim-dir except for compiled AHDL objects (they have to be different)
        print('populating simulation dir') if self.verbosity >= 4 else None
        items = [os.path.join(cache_target, item) for item in os.listdir(cache_target)]
        for item in items:
            if os.path.split(item)[-1] == local_ship_db_relpath:
                shutil.copytree(item, os.path.join(self.working_path, local_ship_db_relpath))
            elif os.path.isdir(item):
                # shutil.copytree(item, os.path.join(self.working_path, os.path.split(item)[-1]), copy_function=os.link)
                ## above not py2.7 compatible
                os.system('cp -Rl {} {}'.format(item, os.path.join(self.working_path, os.path.split(item)[-1])))
            else:
                os.link(item, os.path.join(self.working_path, os.path.split(item)[-1]))

        # write netlist explicity... just incase
        with open(os.path.join(self.working_path, str(self.circuit_id)+'.scs'), 'w') as f:
            f.write(self.circuit.netlist)

        self.original_simdir = self.simdir
        self.simdir = self.working_path

        if _spectre_managed_models:
            self.spicefilepath = os.path.join(cache_target, os.path.split(self.spicefilepath)[-1])
        else:
            self.spicefilepath = os.path.join(self.working_path, str(self.circuit_id) + '.scs')

        ahdl_sim_local = os.path.join(self.working_path, local_sim_db_relpath)
        ahdl_ship_local = os.path.join(self.working_path, local_ship_db_relpath)
        os.makedirs(ahdl_sim_local, exist_ok=True)
        os.makedirs(ahdl_ship_local, exist_ok=True)

        # add special environment vars, if needed
        if _spectre_env is not None and not _spectre_env_flag:
            """
            don't use dict here, becuause we're sensitive to order... must execute in order
            """
            for (var, value) in _spectre_env:
                val = os.path.expandvars(value)
                if '$' in val:
                    val = ':'.join([item for item in val.split(':') if '$' not in item])
                print('{}={}'.format(var, val))
                os.environ[var] = val
            _spectre_env_flag = True

        # build up actual command-line call
        if _rcfile is not None:
            invocation = 'bash --noprofile --norc -ic \'stty -icanon && source {} && '.format(_rcfile)
        else:
            invocation = 'bash --noprofile --norc -ic \'stty -icanon && '

        cmd_args = [
            '-64',
            '+interactive=skill',
            '-log -note -info -debug -warn -error ++aps=moderate',
            '-format=psfbin',
            '-mt',
        ]

        if self.spectre_args:
            if isinstance(self.spectre_args, str):
                cmd_args.append(self.spectre_args)
            elif isinstance(self.spectre_args, (list, tuple)):
                cmd_args.extend(list(self.spectre_args))

        if self.logging:
            cmd_args.append('+log=\"spectre.log\"')

        self.cmd = ' '.join([
            'cd {}'.format(self.working_path),
            '&&',
            'CDS_AHDLCMI_SHIPDB_DIR={}'.format(os.path.join(self.working_path, local_ship_db_relpath)),
            'CDS_AHDLCMI_SIMDB_DIR={}'.format(os.path.join(self.working_path, local_sim_db_relpath)),
            'CDS_AHDLCMI_SHIPDB_COPY=YES',
            _spectre_binary,
            '-outdir {}'.format(self.working_path),
            ' '.join(cmd_args),
            # '-raw=\"raw\"',
            self.spicefilepath
        ])

        full_cmd = invocation + self.cmd + '\''

        try:
            ootime = time.time()
            if _spectre_managed_models:
                global_manager.global_lock().acquire(timeout=120)
                otime = time.time()
                print('spectre got lock in {:6.1f} ms'.format((otime - ootime) * 1000)) if self.verbosity >= 2 else None

            else:
                self._load_relevant_lib_items()

            #### don't set variables in actual environment.... they get confused in parallelism!!!!!
            # os.environ['CDS_AHDLCMI_SIMDB_DIR'] = ahdl_sim_local
            # os.environ['CDS_AHDLCMI_SHIPDB_DIR'] = ahdl_ship_local
            # os.environ['CDS_AHDLCMI_SHIPDB_COPY'] = 'YES'

            if 'CDS_LIC_FILE' in os.environ:
                port, lic_srv = os.environ['CDS_LIC_FILE'].split('@')
                assert not os.system(
                    'ping -c 1 -W 1 {} > /dev/null'.format(lic_srv)), "couldn't reach cadence license server"
            else:
                print(
                    'cadence license server address not in shell env. trying \'spectre\' command anyway') if self.verbosity >= 2 else None

            if self.asynch_repl in ['thread', 'threading']:
                server_process = False
            elif self.asynch_repl in ['process', 'processing', 'multiprocessing']:
                server_process = True
            else:
                server_process = None

            print('starting spectre binary') if self.verbosity >= 4 else None
            self.spectre = get_wrapped_spectre(
                cmd_or_spawn=full_cmd,
                asynch_repl_process=server_process,
                verbosity=self.verbosity,
                shutdown_event=self.shutdown_event,
            )

            self.fresh = False
            self.interface = Accelerated_scl_interface(self)
            self.spectre_pid = self.spectre.spectre_pid
            self.bash_pid = self.spectre.bash_pid
            self.commandct = 0
            self._resolve_analysis_type()

            if _spectre_managed_models:
                scllogfile = os.path.join(cache_target,
                                          '.' + os.path.split(self.spicefilepath)[1].split('.scs')[0] + '.scllog')

                if os.path.isfile(scllogfile):
                    os.link(scllogfile, os.path.join(self.working_path, 'scllog.scllog'))
                    os.remove(scllogfile)
                else:
                    raise RuntimeError('spectre did not create expected scllog file: {}'.format(scllogfile))

            startup_time = time.time() - ootime
            if startup_time < 1:
                print('[spectre] started spectre in {:6.1f} ms'.format(startup_time * 1000)) if self.verbosity >= 2 else None
            elif startup_time >= 1:
                print('[spectre] started spectre in {:6.1f} s'.format(startup_time)) if self.verbosity >= 2 else None

        finally:
            if _spectre_managed_models:
                global_manager.global_lock().release()
            else:
                self._cache_local_ship_db()

    def _cache_local_ship_db(self):
        local_ship_db = os.path.join(self.working_path, local_ship_db_relpath)
        local_lib_contents = os.listdir(local_ship_db)
        library_path = os.path.join(pyspectre_ckt_dir, self.circuit_id, local_ship_db_relpath)
        if not os.path.isdir(library_path):
            os.makedirs(library_path)

        def cache_item(s, d):

            if os.path.isfile(s):
                shutil.copy(s, d)

                # while not os.path.isfile(d):
                #     time.sleep(0.001)

                # with open(s, mode='rb') as ff:
                #     contents = ff.read()
                # contents = contents.replace(self.simid.encode(), self.circuit_id.encode())
                # with open(d, mode='wb') as ff:
                #     ff.write(contents)

            else:
                shutil.copytree(s, d)

                # while not os.path.isdir(d):
                #     time.sleep(0.001)

                # os.makedirs(d)
                # for root, sdirs, files, in os.walk(s):
                #     libroot = root.split(s)[-1].lstrip('/')
                #
                #     for di in sdirs:
                #         os.makedirs(os.path.join(d, libroot, di))
                #
                #     for fi in files:
                #         ss = os.path.join(root, fi)
                #         dd = os.path.join(d, libroot, fi)
                #         with open(ss, mode='rb') as ff:
                #             contents = ff.read()
                #         contents = contents.replace(self.simid.encode(), self.circuit_id.encode())
                #         with open(dd, mode='wb') as ff:
                #             ff.write(contents)

        for local_item in local_lib_contents:
            local_alias = local_item.split('_')
            local_alias = '_'.join(local_alias[:4] + local_alias[5:])

            library_contents = os.listdir(library_path)
            if any([local_alias == '_'.join(item.split('_')[:4] + item.split('_')[5:]) for item in library_contents]):
                continue

            global_manager.global_lock().acquire()  # get a lock
            library_contents = os.listdir(library_path)
            if any([local_alias == '_'.join(item.split('_')[:4] + item.split('_')[5:]) for item in library_contents]):
                # somebody did it while you were waiting!!
                global_manager.global_lock().release()
                continue
            try:
                s = os.path.join(local_ship_db, local_item)
                d = os.path.join(library_path, local_item)
                cache_item(s, d)
                print('cached AHDL model: {}'.format(d))
            finally:
                global_manager.global_lock().release()

    def _load_relevant_lib_items(self):
        loaded = False

        local_ship_db = os.path.join(self.working_path, local_ship_db_relpath)
        # everything should already be copied, just have to fix names and contents:

        original_ids = list(set([item.split('_')[4] for item in os.listdir(local_ship_db)]))
        original_ids = [item for item in original_ids if len(item) == len(self.simid) and '_' not in item]

        # see what needs to be renamed

        for id in original_ids:

            pairs = []

            for root, sdirs, files in os.walk(local_ship_db):
                for sdir in sdirs:
                    if id in sdir:
                        pairs.append((
                            os.path.join(root, sdir),
                            os.path.join(root, sdir.replace(id, self.simid))
                        ))
                for ffile in files:
                    if id in ffile:
                        pairs.append((
                            os.path.join(root, ffile),
                            os.path.join(root, ffile.replace(id, self.simid))
                        ))

            if pairs:
                loaded = True

                # rename everything
                for s, d in pairs:
                    shutil.move(s, d)

                # now go through and fix contents
                for _, dd in pairs:
                    if os.path.isfile(dd):
                        with open(dd, mode='rb') as ff:
                            contents = ff.read()
                        contents = contents.replace(id.encode(), self.simid.encode())
                        with open(dd, mode='wb') as ff:
                            ff.write(contents)

                    else:
                        for root, _, files in os.walk(dd):
                            for ffile in files:
                                with open(os.path.join(root, ffile), mode='rb') as ff:
                                    contents = ff.read()
                                contents = contents.replace(id.encode(), self.simid.encode())
                                with open(os.path.join(root, ffile), mode='wb') as ff:
                                    ff.write(contents)

        return loaded
        # library_path = os.path.join(pyspectre_ckt_dir, self.circuit_id, local_ship_db_relpath)
        # if not os.path.isdir(library_path):
        #     return
        # library_contents = os.listdir(library_path)
        # relevent_lib_items = [item for item in library_contents if self.circuit_id in item]
        #
        # if relevent_lib_items:
        #     # found relevant library item... copy it into the local library
        #     for item in relevent_lib_items:
        #         newname = item.replace(id_rep, path_rep)
        #         s = os.path.join(pyspectre_ahdl_lib, item)
        #         d = os.path.join(local_ship_db, newname)
        #         if os.path.isdir(s):
        #             shutil.copytree(s, d)
        #         elif os.path.isfile(s):
        #             shutil.copy(s, d)
        #
        #             if any([re.match(pattern, item) for pattern in
        #                     _get_patterns_for_internal_replacement(id_rep)]):
        #                 contents = open(d, mode='rb').read()
        #                 contents = contents.replace(id_rep.encode(), path_rep.encode())
        #                 open(d, mode='wb').write(contents)

    def _process_netlist_file(self, filepath):
        netlist_path = list(os.path.split(filepath))
        netlist_file = netlist_path.pop()
        netlist_path = os.path.join(*netlist_path)

        netlist_name = list(netlist_file.split(os.extsep))
        netlist_ext = netlist_name.pop()
        netlist_name = ''.join(netlist_name)

        target_file = os.path.join(
                netlist_path,
                modfile_prefix + netlist_name + modfile_suffix + os.extsep + netlist_ext)

        with open(filepath, 'r') as f:
            text = f.read()

        text = self._process_netlist_text(text)

        with open(target_file, mode='w') as f:
            f.write(text)

        return target_file

    def _process_netlist_text(self, text):

        if self.pwl_inputs is not None:
            timepoint_reservation = max([val.max_n_points for val in self.pwl_inputs])
            targets = [item.cname for item in self.pwl_inputs]
            text = extend_pwl_defs_in_netlist(
                text,
                targets,
                timepoint_reservation,
            )

        if self.strobe_interval is not None:
            new_anal = gen_transient_analysis_block(
                transient_analysis_name,
                strobe_period=self.strobe_interval)
            text = insert_analysis_block(text, new_anal)

        elif self.strobetimes is not None:
            self.strobe_reservation_size = 3*max([val.max_n_points for val in self.pwl_inputs])
            new_anal = gen_transient_analysis_block(
                    transient_analysis_name,
                    strobetimese=np.cumsum([]*self.strobe_reservation_size)
                )
            text = insert_analysis_block(text, new_anal)

        return text

    def _read_file(self, filename, signames='all'):
        if not os.path.isfile(filename):
            filename = os.path.join(self.working_path, 'psf', filename)

        if isinstance(signames, str) and not signames == 'all':
            signames = [signames]

        if self.reader == 'libpsf':
            try:
                d = libpsf.PSFDataSet(filename)
            except IOError as e:
                print(
                    'ERROR: libpsf couldn\'t open file: '
                    '{}. See spectre simulation directory: {}'.format(filename, self.working_path)
                )
                raise e

            if isinstance(signames, str) and signames == 'all':
                return d.get_sweep_values().flatten(), np.array([d.get_signal(signame)
                                                                 for signame in d.get_signal_names()]).T
            else:
                return d.get_sweep_values().flatten(), np.array([d.get_signal(signame)
                                                                 for signame in signames]).T

        else:
            raise NotImplementedError

    def _resolve_analysis_type(self):
        self.analysis_type = self.interface.listanalysis()[self.analysis_name]
        # self.analysis = eval('Analyses.' + self.analysis_type.title() + '()')

    def getparams(self, obj, paramnames):
        if isinstance(paramnames, str):
            paramnames = [paramnames]
        paramlist = self.interface.listparams(obj)
        if not all([name in paramlist for name in paramnames]):
            missing = [name not in paramlist for name in paramnames]
            print('coudln\'t find parameters named:{} in object:{}'.format(missing, obj))
            raise PyspectreUnresolvableError
        return {name: self.interface.getparametervalue(obj, name) for name in paramnames}

    def setparams(self, obj, paramdict):
        assert isinstance(paramdict, dict)
        for name, value in paramdict.items():
            self.interface.setparametervalue(obj, name, value)

    def setstrobe(self, analysis, strobe_times):
        strobe_times.sort()

        # must overwrite all reserved values!
        addnl_ct = self.strobe_reservation_size - len(strobe_times)
        addnl_ts = strobe_times[-1] - strobe_times[-2]
        extension = strobe_times[-1] + np.cumsum([addnl_ts]*addnl_ct)
        strobe_times = np.r_[strobe_times, extension]

        assert len(strobe_times) == self.strobe_reservation_size
        assert all(np.diff(strobe_times) > 0), 'provided strobetime values are not monotonic'

        try:
            self.setparams(analysis, {'strobetimes': format_vect(strobe_times)})
        except:
            raise RuntimeError('could not set strobe property. Have AHDL models been compiled?')

    def setpwl(self, name, time_vec, volts):

        if isinstance(name, PwlInput):
            name = name.cname

        assert len(time_vec.shape) <= 2
        if len(time_vec.shape) == 2:
            assert np.min(time_vec.shape) == 1
            time_vec = time_vec.squeeze()

        assert len(volts.shape) <= 2
        if len(volts.shape) == 2:
            assert np.min(volts.shape) == 1
            volts = volts.squeeze()

        assert time_vec.size == volts.size, 'incompatible time vector'

        as_text = format_vect(np.stack([time_vec, volts]))

        self.setparams(name, {'wave': as_text})

    def close(self):
        self._cleanup()

    def runanalysis(self, time_vec, stimuli, analysis_name=None, signal_names='all', strobe=True, jacobian=None,
                    resample=False, dtype='float32', verbosity=1):

        otime = time.time()

        if analysis_name is None:
            if self.analysis_name is not None:
                analysis_name = self.analysis_name
            else:
                transient_analysis_name
                raise ValueError('must specify an analysis name')

        try:

            for i, input in enumerate(self.pwl_inputs):
                self.setpwl(input, time_vec=time_vec, volts=stimuli[:, i])

            if strobe and strobe != True and not strobe == self.strobe:
                try:
                    self.setstrobe(analysis_name, np.array(list(time_vec)))
                except RuntimeError as e:
                    self.quit = True
                    return None, None

            self.setparams(analysis_name, {'stop': time_vec[-1]})

            self.interface.runanalysis(analysis_name)

            restime, signals = self.read_results(analysis_name=analysis_name, signal_names=signal_names)

            if resample:
                n_blocks = time_vec.size

                t_len = int(
                    (np.floor(restime.size / n_blocks)
                     if not np.floor(restime.size / n_blocks) % 2 else np.floor(restime.size / n_blocks) - 1
                     ) * n_blocks)

                if t_len == 0:
                    t_len = int(
                        (np.ceil(restime.size / n_blocks)
                         if not np.ceil(restime.size / n_blocks) % 2 else np.ceil(restime.size / n_blocks) + 1
                         ) * n_blocks)

                reg_samps = interp1d(
                    x=restime,
                    y=signals,
                    kind='linear',
                    axis=0,
                    bounds_error=False,
                    fill_value=(signals[0, :], signals[-1, :]),
                    assume_sorted=True,
                )(np.linspace(time_vec[0], time_vec[-1], t_len, endpoint=True))

                block_size = int(reg_samps.shape[0] / n_blocks)
                split_inds = np.cumsum(
                    np.stack([block_size] * n_blocks),
                )[:-1].astype(int)
                new_sigs = np.stack([
                    np.mean(item, axis=0) for item in np.array_split(reg_samps, split_inds, axis=0)
                ])

                signals = new_sigs
                restime = time_vec

            else:
                pass

            signals = signals.astype(dtype=dtype)
            restime = restime.astype(dtype=dtype)
            print('simulation took {:4.1f} ms'.format((time.time() - otime) * 1000)) if verbosity >= 2 else None
            return restime, signals

        except KeyboardInterrupt as e:
            raise e

        except InterruptedError as e:
            raise e

        except Exception as e:
            print('analysis failed')
            if self.logging:
                shutil.copytree(self.working_path, os.path.join(pyspectre_path, 'crashed', self.simid))
                print('copied simdir to {} for debugging'.format(os.path.join(pyspectre_path, 'crashed', self.simid)))
            self._cleanup()
            raise e


    def setresultsdir(self, relpath=None):
        if relpath is None:
            if self.results_dir is not None:
                relpath = self.results_dir
            else:
                raise ValueError('no path specified')

        self.interface.setresultsdir(relpath)

    def readstatefile(self, filename=None):
        statedata = {}
        with open(os.path.join(self.working_path, filename)) as rdata:
            for line in rdata:
                if line.startswith('#'):
                    continue
                else:
                    tok = line.split()
                    statedata.update({tok[0]: float(tok[1])})

        return statedata

    def readdcfile(self):
        return self.readstatefile(self.analysis.dcfile)

    def readtranfile(self):
        return self.readstatefile(self.analysis.tranfile)

    def init(self, *args, **kwargs):
        """ use this for one-time calls which will persist forever"""
        self.analysis.init(self, *args, **kwargs)
        return self.analysis.dcfile

    def reset(self, *args, **kwargs):
        """ use this for one-time prep calls which will persist between analyses"""
        self.analysis.reset(self, *args, **kwargs)
        return self.analysis.tranfile

    def step(self, *args, **kwargs):
        """ use this for calls which have to be made before every step"""
        self.analysis.step(self, *args, **kwargs)
        return self.analysis.tranfile

    def get_data(self, signames=None):
        if isinstance(signames, (str)):
            signames = [signames]
        t, x = self.analysis.interpret_data(self, signames)
        return t, x

    def read_results(self, signal_names='all', analysis_name=None, analysis_type=None):
        if analysis_name is None:
            if self.analysis_name is not None:
                analysis_name = self.analysis_name
            else:
                raise ValueError('must specify analysis name')

        if analysis_type is None:
            if self.analysis_type is not None:
                analysis_type = self.analysis_type
            else:
                raise ValueError('must specify analysis type')

        return self._read_file(
            os.path.join(self.results_dir, '.'.join([analysis_name, analysis_type])),
            signames=signal_names
        )

    def run_batch(self, list_of_stimuli, list_of_times, signal_names='all', dtype='float64',
        resample=False, verbosity=None, analysis_name=None, strobe=None):

        if strobe is None:
            if self.strobe is not None:
                strobe = self.strobe
            else:
                strobe = True

        if analysis_name is None:
            analysis_name = self.analysis_name

        if verbosity is None:
            if self.verbosity is not None:
                verbosity = self.verbosity
            else:
                verbosity = 1

        if len(list_of_times.shape) == 1:
            list_of_times = np.expand_dims(list_of_times, 0)
        if len(list_of_times.shape) > 2:
            raise ValueError('[pyspectre] Time vector must be 1 or 2 dimensional')
        if 1 in list_of_times.shape:
            list_of_times = list_of_times.reshape(1, -1)
            list_of_times = np.broadcast_to(list_of_times, (list_of_stimuli.shape[0], list_of_times.shape[1]))

        assert list_of_stimuli.shape[0] == list_of_times.shape[0]
        assert list_of_stimuli.shape[1] == list_of_times.shape[1]

        print('[pyspectre] starting batch of {} simulations.'.format(len(list_of_stimuli))) if verbosity >= 1 else None
        restimes = [None]*list_of_stimuli.shape[0]
        ressignals = [None]*list_of_stimuli.shape[0]

        if not all([np.all(tt == list_of_times[0]) for tt in list_of_times]) and strobe is not None:
            print('[pyspectre] WARNING: dissimilar time vectors in single instance of Pyspectre with strobing enabled') if self.verbosity >= 1 else None

        otime = time.time()
        for i, (time_vec, stimulus) in enumerate(zip(list_of_times, list_of_stimuli)):

            try:
                restimes[i], ressignals[i] = self.runanalysis(
                    time_vec, stimulus, analysis_name=analysis_name,
                    signal_names=signal_names, strobe=strobe, resample=resample, dtype=dtype,
                    verbosity=verbosity)

            except Exception as e:
                if not self.spectre.is_alive or self.spectre.fatal_error:
                    print(e)
                    return restimes, ressignals
                else:
                    pass

        r_time = time.time() - otime

        if r_time < 1:
            print('[pyspectre] batch took {:4.1f} ms'.format(r_time * 1000)) if verbosity >= 1 else None
        elif 60 > r_time >= 1:
            print('[pyspectre] batch took {:4.1f} s'.format(r_time)) if verbosity >= 1 else None
        elif r_time >= 60:
            print('[pyspectre] batch took {:4.1f} m'.format(r_time / 60)) if verbosity >= 1 else None

        if strobe and not all([r.size == itt.size for itt,r in zip(list_of_times, restimes)]):
            print('[pyspectre] output time length not equal to input time length')
        return restimes, ressignals
