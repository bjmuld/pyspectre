import multiprocessing as mp
import threading
import sys
import time
import os

if sys.version_info.major == 3:
    import queue as queue
elif sys.version_info.major == 2:
    import Queue as queue

from pexpect.replwrap import REPLWrapper


def kill_pid(pid, sig):
    while True:
        try:
            os.kill(pid, sig)
            time.sleep(0.5e-3)
        except:
            return


def get_wrapped_spectre(
    cmd_or_spawn,
    asynch_repl_process=False,
    verbosity=1,
    shutdown_event=None,
):

    if asynch_repl_process is None:
        return REPLWrapped_spectre(
            cmd_or_spawn=cmd_or_spawn
        )

    elif asynch_repl_process:
        return AsyncWrappedSpectre(
            cmd_or_spawn=cmd_or_spawn,
            server_process=True,
            verbosity=verbosity,
            shutdown_event=shutdown_event,
        )

    elif not asynch_repl_process:
        return AsyncWrappedSpectre(
            cmd_or_spawn=cmd_or_spawn,
            server_process=False,
            verbosity=verbosity,
            shutdown_event=shutdown_event,
        )


class REPLWrapped_spectre(REPLWrapper):

    def __init__(
            self,
            cmd_or_spawn,
    ):

        try:
            super(REPLWrapped_spectre, self).__init__(
                cmd_or_spawn=cmd_or_spawn,
                orig_prompt=u'\n> ',
                prompt_change=None,
            )

        except Exception as e:
            if hasattr(self, 'child') and self.child is not None:
                print(self.child.before)
            raise e

        self.child.delayafterclose = 0.0
        self.child.delaybeforesend = 0.0
        self.child.delayafterread = 0.0
        self.child.delayafterterminate = 0.0
        self.spectre_pid = int(self.run_command('sclGetPid()'))
        if not self.spectre_pid:
            raise RuntimeError
        self.bash_pid = self.child.pid
        self.fatal_error = False

    def __getattr__(self, item):
        if hasattr(self.child, item):
            return getattr(self.child, item)

    @property
    def isalive(self):
        return self.child.is_alive()

    is_alive = isalive
    isAlive = isalive

    def run_command(self, *args, **kwargs):
        try:
            return super(REPLWrapped_spectre, self).run_command(*args, **kwargs)
        except Exception as e:
            self.fatal_error = True
            raise e

    def close(self, force=True):
        """This closes the connection with the child application. Note that
        calling close() more than once is valid. This emulates standard Python
        behavior with files. Set force to True if you want to make sure that
        the child is terminated (SIGKILL is sent if the child ignores SIGHUP
        and SIGINT).

        self.flush()
        with _wrap_ptyprocess_err():
            # PtyProcessError may be raised if it is not possible to terminate
            # the child.
            self.ptyproc.close(force=force)
        self.isalive()  # Update exit status from ptyproc
        self.child_fd = -1
        self.closed = True
        """
        self.child.close(force)

    def terminate(self, force=False):
        """This forces a child process to terminate. It starts nicely with
        SIGHUP and SIGINT. If "force" is True then moves onto SIGKILL. This
        returns True if the child was terminated. This returns False if the
        child could not be terminated.

        if not self.isalive():
            return True
        try:
            self.kill(signal.SIGHUP)
            time.sleep(self.delayafterterminate)
            if not self.isalive():
                return True
            self.kill(signal.SIGCONT)
            time.sleep(self.delayafterterminate)
            if not self.isalive():
                return True
            self.kill(signal.SIGINT)
            time.sleep(self.delayafterterminate)
            if not self.isalive():
                return True
            if force:
                self.kill(signal.SIGKILL)
                time.sleep(self.delayafterterminate)
                if not self.isalive():
                    return True
                else:
                    return False
            return False
        except OSError:
            # I think there are kernel timing issues that sometimes cause
            # this to happen. I think isalive() reports True, but the
            # process is dead to the kernel.
            # Make one last attempt to see if the kernel is up to date.
            time.sleep(self.delayafterterminate)
            if not self.isalive():
                return True
            else:
                return False
        """
        self.child.terminate(force)

    def kill(self, sig):
        """This sends the given signal to the child application. In keeping
        with UNIX tradition it has a misleading name. It does not necessarily
        kill the child unless you send the right signal.

        # Same as os.kill, but the pid is given for you.
        if self.isalive():
            os.kill(self.pid, sig)
        """
        self.child.kill(sig)


class AsyncWrappedSpectre(object):

    def __init__(self,
                 cmd_or_spawn,
                 server_process=False,
                 verbosity=1,
                 shutdown_event=None,
                 ):

        if shutdown_event is not None and shutdown_event.is_set():
            raise KeyboardInterrupt
        self.cmd = cmd_or_spawn
        self.shutdown_event = shutdown_event if shutdown_event is not None else threading.Event()
        self.verbosity = verbosity
        self.server_process = server_process
        self.inqueue = None
        self.outqueue = None
        self.server = None
        self.fatal_error = False

        if not self.server_process:

            self.inqueue = queue.Queue(1)
            self.outqueue = queue.Queue(1)
            self.shutdown_queue = queue.Queue(1)

            self.server = threading.Thread(
                target=self.REPLWrapper_server_fn,
                args=(
                    self.inqueue,
                    self.outqueue,
                    self.shutdown_queue,
                    self.cmd,
                ),
                daemon=True,
            )

        else:

            self.inqueue = mp.Queue(1)
            self.outqueue = mp.Queue(1)
            self.shutdown_queue = mp.Queue(1)

            self.server = mp.Process(
                target=self.REPLWrapper_server_fn,
                args=(
                    self.inqueue,
                    self.outqueue,
                    self.shutdown_queue,
                    self.cmd,
                ),
                daemon=True,
            )

        self.server.start()
        self.bash_pid = int(self._interact(('get_bash_pid', (None,))))
        self.spectre_pid = int(self._interact(('get_spectre_pid', (None,))))
        self.threadName = self.server.name if not self.server_process else None
        self.child = self

    def isalive(self):
        return self.server.is_alive()

    is_alive = isalive
    isAlive = isalive

    def run_command(self, *args, **kwargs):
        return self._eval_remote_method('run_command', args, kwargs)

    def notify_server(self):
        print('[REPLWrapper_client] notify_server()') if self.verbosity >= 4 else None
        while not self.inqueue.empty():
            _ = self.inqueue.get_nowait()
        self.inqueue.put('terminate')
        self.shutdown_queue.put(True)

    # def terminate(self):
    #     # graceful exit
    #     print('[REPLWrapper_client] received terminate() request') if self.verbosity >= 4 else None
    #     self.notify_server()
    #     print('[REPLWrapper_client] joining server_thread') if self.verbosity >= 4 else None
    #     self.server.join()
    #     print('[REPLWrapper_client] joined server_thread') if self.verbosity >= 4 else None

    def kill(self):
        # ungraceful exit
        print('[REPLWrapper_client] received kill() request') if self.verbosity >= 4 else None
        self.notify_server()
        while True:
            try:
                #os.kill(self.spectre_pid, 2)
                #os.kill(self.spectre_pid, 15)
                os.kill(self.spectre_pid, 9)
                os.kill(self.bash_pid, 9)
                time.sleep(10e-3)
            except:
                print('[REPLWrapper_client] killed spectre ({})'.format(self.spectre_pid)) if self.verbosity >= 4 else None
                break
        print('[REPLWrapper_client] joining server thread') if self.verbosity >= 4 else None
        self.server.join()
        print('[REPLWrapper_client] joined server thread') if self.verbosity >= 4 else None

    def _eval_remote_method(self, method_name, args, kwargs):
        payload = ('eval_method', (method_name, args, kwargs))
        return self._interact(payload)

    def _interact(self, payload):
        if not self.is_alive():
            raise RuntimeError

        try:
            self.inqueue.put(payload, block=False)
            print('[REPLWrapper_client] sent payload: {}'.format(payload)) if self.verbosity >= 5 else None
            # break
        except queue.Full:
            # somethings wrong.  this queue should always be empty!
            print('[REPLWrapper_client] inqueue full. rejecting cmd: {}'.format(payload)) if self.verbosity >= 5 else None
            self.kill()
            raise RuntimeError
            # self.shutdown.wait(0.1e-3)

        # the client will be in this loop while spectre is computing stuff:
        while True:
            if not self.server.is_alive() or self.shutdown_event.is_set():
                break
            try:
                result = self.outqueue.get(block=False)
                break
            except queue.Empty:
                self.shutdown_event.wait(0.5e-3)

        if self.shutdown_event.is_set():
            print('[REPLWrapper_client] received shutdown signal') if self.verbosity >= 2 else None
            self.fatal_error = True
            self.kill()
            raise KeyboardInterrupt

        elif not self.server.is_alive():
            try:
                if payload[1][1][0] != 'sclQuit()':
                    print('[REPLWrapper_client] server died during call') if self.verbosity >= 3 else None
            except:
                pass
            self.fatal_error = True
            self.kill()
            raise RuntimeError

        print('[REPLWrapper_client] returning result: {}'.format(result)) if self.verbosity >= 5 else None
        return result

    def REPLWrapper_server_fn(self, inqueue, outqueue, shutdown_queue,
                              cmd_or_spawn):
        try:

            spectre = REPLWrapped_spectre(cmd_or_spawn)

            def try_kill():
                kill_pid(spectre.spectre_pid, 2)
                print('[REPLWrapper_server_fn] killed {}'.format(spectre.spectre_pid)) if self.verbosity >= 4 else None
                if spectre.isalive():
                    spectre.close()
                print('[REPLWrapper_server_fn] waiting to join repl process') if self.verbosity >= 3 else None
                spectre.wait()
                print('[REPLWrapper_server_fn] joined repl process') if self.verbosity >= 3 else None

            print('[REPLWrapper_server_fn] started') if self.verbosity >= 3 else None
            print('[REPLWrapper_server_fn] spectre PID: {}'.format(spectre.spectre_pid)) if self.verbosity >= 3 else None
            print('[REPLWrapper_server_fn] bash PID: {}'.format(spectre.bash_pid)) if self.verbosity >= 3 else None

            for payload in iter(inqueue.get, 'terminate'):

                if not spectre.child.isalive():
                    raise RuntimeError('[REPLWrapper_server_fn] you asked for something from a dead spectre')

                # pull out fn from args
                assert isinstance(payload, tuple) and len(payload) == 2
                fnselect = payload[0]
                fnargs = payload[1]

                # define functions:
                if fnselect == 'eval_method':
                    command = fnargs[0]
                    args = fnargs[1]
                    kwargs = fnargs[2]

                    outqueue.put(getattr(spectre, command)(*args, **kwargs), block=True, timeout=None)

                if fnselect == 'get_bash_pid':
                    outqueue.put(spectre.bash_pid)

                if fnselect == 'get_spectre_pid':
                    outqueue.put(spectre.spectre_pid)

            print('[REPLWrapper_server_fn] received \'terminate\' in queue') if self.verbosity >= 3 else None
            raise EOFError

        except EOFError:
            pass

        except KeyboardInterrupt:
            pass

        except Exception as e:

            exception_handled = False

            print('[REPLWrapper_server_fn] exception raised in replServer') if self.verbosity >= 3 else None

            def handle_msg(exception_type):
                print('[REPLWrapper_server_fn] exception \'{}\' handled by replServer'.format(
                    exception_type)) if self.verbosity >= 3 else None

            if 'command' in locals() and command == 'run_command' and 'args' in locals() and args[0] == 'sclQuit()':
                handle_msg('sclQuit')
                exception_handled = True

            elif self.shutdown_event is not None and self.shutdown_event.is_set():
                handle_msg('shutdown_event')
                exception_handled = True

            elif not inqueue.empty() and inqueue.get_nowait() == 'terminate':
                handle_msg('terminate in queue')
                exception_handled = True

            elif not self.shutdown_queue.empty():
                handle_msg('shutdown_queue')
                exception_handled = True

            if not exception_handled:
                self.fatal_error = True
                print('[REPLWrapper_server_fn] exception not handled') if self.verbosity >= 3 else None
                # try:
                #     print('[REPLWrapper_server_fn] {}'.format(spectre.child.before)) if self.verbosity >= 4 else None
                # except:
                #     pass
                try:
                    print('[REPLWrapper_server_fn] unhandled exc: {}'.format(e)) if self.verbosity >= 4 else None
                except:
                    pass
                raise e

        finally:
            if 'try_kill' in locals():
                try_kill()
