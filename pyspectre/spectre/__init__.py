import re
import numpy as np
from textwrap import wrap as textwrap

float_fmt_string = '{:+13.6e}'


def format_float(float):
    if isinstance(float, np.ndarray):
        assert float.size == 1
        float = float[0]
    return float_fmt_string.format(float)


def format_vect(vect):
    return "[{}]".format(' '.join([format_float(v) for v in vect.T.flatten()]))


def extend_pwl_defs_in_netlist(text, targets, n_points):
    """
    create an alterate version of the netlist with reservation in the wave="*" assignment
    for a pwl signal as long as was requested
    :return:
    """

    target_names = [item.split('.')[-1] for item in targets]
    if not isinstance(text, (tuple, list)):
        text = text.split('\n')

    # go through and find definitions:
    def_lines = {}
    context = []
    for line_no, line in enumerate(text):
        line = line.lstrip()
        if line.startswith('//'):
            continue
        toks = line.split()
        if len(toks) < 1:
            continue
        anchor = toks[0]
        if anchor == 'subckt':
            context.append(toks[1])
            continue
        if anchor == 'ends':
            context.pop()
            continue
        if anchor in target_names:
            this_guys_name = '.'.join(context) + '.' + anchor if context else anchor
            if this_guys_name in targets:
                stopline = line_no
                while re.match(r'.*\\\s*$', text[stopline]):
                    stopline += 1
                def_lines.update({this_guys_name: {'PwlInput': target_names.index(anchor),
                                                   'range': range(line_no, stopline + 1)}})

    assert all([item in def_lines for item in targets])

    file_pattern = r'^.*(\s+file=\".+?\")'
    wave_pattern = r'^.*(\s+wave=[\[\"].+?[\]\"])'

    for name, patch_dict in def_lines.items():

        fulldef = ' '.join([text[ll].replace('\\', '').strip() for ll in patch_dict['range']])

        # remove all 'file' definitions

        if re.match(file_pattern, fulldef):
            fulldef = ' '.join(
                [item.strip() for item in fulldef.split(re.match(file_pattern, fulldef).groups()[0])]).strip()

        # check and extend existing 'wave' definition
        if re.match(wave_pattern, fulldef):
            oldwave = re.match(wave_pattern, fulldef).groups()[0].split('=')[1].strip().strip('\"[]').split()
            fulldef = ' '.join(
                [item.strip() for item in fulldef.split(re.match(wave_pattern, fulldef).groups()[0])]).strip()
            old_time = np.stack([float(t) for t in oldwave[::2]])
            old_vals = np.stack([float(v) for v in oldwave[1::2]])

            if old_time.size >= n_points:
                continue
            else:
                aug_len = n_points - old_time.size
                ts = np.max(np.diff(old_time))
                new_time = np.concatenate(
                    [old_time, np.linspace(old_time[-1] + ts, old_time[-1] + aug_len * ts, aug_len)])
                new_vals = np.concatenate([old_vals, np.zeros(aug_len)])

                newval_str = ' '.join(
                    [' '.join([float_fmt_string] * 2).format(new_time[i], new_vals[i]) for i in range(aug_len)])

        else:
            # create brand-new 'wave' definition
            aug_len = n_points
            ts = 1
            new_time = np.linspace(0, (aug_len - 1) * ts, aug_len)
            new_vals = np.random.randn(aug_len)

            newval_str = ' '.join(
                [' '.join([float_fmt_string] * 2).format(new_time[i], new_vals[i]) for i in range(aug_len)])

        fulldef = ' '.join([fulldef, 'wave=[' + newval_str + ']'])
        fulldef = textwrap(fulldef, break_long_words=False, break_on_hyphens=False, subsequent_indent='    ')
        fulldef[:-1] = [item + ' \\' for item in fulldef[:-1]]  ## fwd slash on ends of lines
        # fulldef[1:] = ['+ ' + item for item in fulldef[1:]]     ## '+' at beginning of lines
        def_lines[name].update({'def': [ll + '\n' for ll in fulldef]})

    # all fixed up!
    # shutil.copy(self.spicefilepath, self.spicefilepath+'_bk')
    patchlist = [item['range'] for item in def_lines.values()]

    new_text = []

    for line_no, line in enumerate(text):
        if any([line_no in r for r in patchlist]):
            patch = [v for v in def_lines.values() if line_no in v['range']][0]
            if line_no + 1 not in patch['range']:
                # outfile.writelines(patch['def'])
                new_text.extend(patch['def'])
                del patchlist[patchlist.index(patch['range'])]
        else:
            new_text.append(line + '\n')

    return ''.join(new_text)


def gen_transient_analysis_block(name, strobe_period=None, strobetimes=None):

    if strobetimes:

        return "{} tran stop=1 errpreset=liberal ic=dc skipdc=no readic=\"tran.ss\" readns=\"tran.ss\" \\\n" \
               "    write=\"tran.ss\" method=gear2 writefinal=\"tran.fc\" save=all \\\n" \
               "    strobetimes={}".format(
                name,
                format_vect(strobetimes),
        )

    elif strobe_period:

        return "{} tran stop=1 errpreset=liberal ic=dc skipdc=no readic=\"tran.ss\" readns=\"tran.ss\" \\\n" \
               "    write=\"tran.ss\" method=gear2 writefinal=\"tran.fc\" save=all strobeperiod={} \n" \
               "".format(name, format_float(strobe_period))


def insert_analysis_block(text, analysis_block):
    block_name = analysis_block.split()[0]

    out_text = []
    inside_it=False
    for line in text.split('\n'):
        toks = line.split()

        if inside_it:
            if toks[-1] == '\\':
                pass
            else:
                inside_it = False

        elif not inside_it:
            if len(toks) > 0 and toks[0] == block_name:
                inside_it = True
            else:
                out_text.append(line)

    out_text = ('\n'.join(out_text)).rstrip() + '\n' + analysis_block + '\n'

    return out_text


# def get_REPLWrapper_in_this_process(
#         cmd_or_spawn,
#         orig_prompt,
#         prompt_change=None):
#
#     from pexpect.replwrap import REPLWrapper
#
#     try:
#         a = REPLWrapper(
#                 cmd_or_spawn,
#                 orig_prompt,
#                 prompt_change)
#
#         a.child.delayafterclose = 0.0
#         a.child.delaybeforesend = 0.0
#         a.child.delayafterread = 0.0
#         a.child.delayafterterminate = 0.0
#
#         return a
#
#     except Exception as e:
#         if 'a' in locals() and a is not None:
#             print(a.child.before)
#         raise e


# def get_REPLWrapper_in_another_process(
#         cmd_or_spawn,
#         orig_prompt,
#         prompt_change=None):
#
#     from .replwrap_server import REPLWrapper_client
#
#     return REPLWrapper_client(
#         cmd_or_spawn,
#         orig_prompt,
#         prompt_change)


# try:
#     # self.spectre = REPLWrapper(
#     #     cmd_or_spawn=full_cmd,
#     #     orig_prompt=u'\n> ',
#     #     prompt_change=None,
#     # )
#     self.spectre = REPLWrapper_client(
#         cmd_or_spawn=full_cmd,
#         orig_prompt=u'\n> ',
#         prompt_change=None, )
#
# except BaseException as e:
#     if self.spectre is not None:
#         print(self.spectre.child.before)
#     self._cleanup()
#     raise e