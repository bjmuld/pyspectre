#!/usr/bin/env python

from setuptools import setup, find_packages
import os
import re
import inspect

try:
    long_desc = open('README.md', 'r').read()

except :
    long_desc = 'pyspectre provides an interactive python interface to spectre simulator'

# get the full path to this file :
thispath = os.path.abspath(os.path.realpath(os.path.split(inspect.stack()[0][1])[0]))

# read version from /pyspectre/__init__.py :
for line in open(os.path.join(thispath, 'pyspectre/__init__.py'), 'r').readlines():                ##_xanity_replace
    scraped = re.search(r'\s*__version__\s*=\s*[\"\'](\S+)[\"\']', line)
    if scraped:
        version = scraped.group(1).strip('\'\"').strip()


setup(name='pyspectre',
      version=version,
      description='python interface to supervise and control single instance of Spectre circuit simulator',
      long_description=long_desc,
      long_description_content_type="text/markdown",
      url='http://github.gatech.edu/lars/pyspectre',
      author='Barry Muldrey',
      author_email='bmuldrey3@gatech.edu',
      license='MIT',
      packages=find_packages(),
      install_requires=['pexpect', 'numpy', 'libpsf', 'waverunner', 'scipy'],
      # extras_require={'server': ['waverunner', 'xarray', 'scipy']},
      entry_points={
          'console_scripts': [
              'pyspectre_server = pyspectre.server:main [server]',
          ]
      }
      )
